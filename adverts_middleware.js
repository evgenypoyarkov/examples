/* прослойка для подбора объявлений по заданным парамерам таргетинга */

function getAdverts(req, res, next) {
  var target = req.target;
  var themesTotal = target.themes.length;
  res.adverts = [];
  res.advertsIDs = [];

  if (target.themes.length === 0)
    return next();

  var done = function() {
    return res.adverts.length === req.adsRequired;
  };

  var isLastTheme = function(id) {
    return target.themes.indexOf(id) === (themesTotal - 1);
  };

  var temporaryStorageKey = 'temp:'
    + utils.hashMD5(target.country + target.site + Date.now());

  function filter(step, callback) {
    var useGeo = step.useGeo;
    var useTime = step.useTime;
    console.log('FILTER:', arguments);

    /* 1: get all adverts that could be paid */
    redis.sinterstore(temporaryStorageKey, temporaryStorageKey, 
                      'adverts:allowed');

    /* 2: apply filter by targeting */
    if (useGeo)
      redis.sinterstore(temporaryStorageKey, temporaryStorageKey,
                        'target:geo:' + target.country + ':adverts');
    if (useTime) {
      var bounds = window.getTimeBounds();
      if (!bounds.upper || !bounds.lower)
        callback(true); /* emit an error */

      redis.sinterstore(temporaryStorageKey, temporaryStorageKey,
                        'target:time:' + bounds.lower + ':adverts',
                        'target:time:' + bounds.upper + ':adverts');
    }

    /* 3: get filtered adverts */
    redis.smembers(temporaryStorageKey, callback);
  }

  var Step = function(useGeo, useTime) {
    this.useGeo = useGeo;
    this.useTime = useTime;
  };

  var multi = redis.multi();

  var log = function(err) {
    console.log(arguments);
    if (err)
      return res.end();
  };

  async.eachSeries(target.themes, function(themeID, callback) {
    console.log('New cycle. theme:', themeID);
    var steps = [
      new Step(true, true),
      new Step(true, false),
      new Step(false, true),
      new Step(false, false)
    ];
    async.eachSeries(steps, function(step, callback) {
      console.log('New cycle. theme:', themeID, step);
      async.waterfall([
        function(callback) {
          var q = ['theme:' + themeID + ':adverts:prices', 0, -1];
          redis.zrevrange(q, callback);
        },
        function(adverts, callback) {
          if (!adverts.length)
            return callback(null, []);

          async.series([
            function(callback) {
              redis.sadd(temporaryStorageKey, adverts, callback);
            },
            function(callback) {
              filter(step, callback);
              redis.expire(temporaryStorageKey, 10);
            }
          ], callback);
        }
      ],
      function(err, result) {
        console.log(arguments);
        if (err)
          return callback(err);
        var data = (result && result.length === 2) ? result[1] : [];
        for (var i = 0, len = data.length; i < len && !done(); ++i) {
          if (res.advertsIDs.indexOf(data[i]) === -1) {
            res.advertsIDs.push(data[i]);
            res.adverts.push({
              advertID: data[i],
              foundInTheme: themeID,
            });
          }
        }
        if (done() || (!step.useGeo && !step.useTime && isLastTheme(themeID))) {
          return next();
        }
        else
          callback();
      });
    }, callback);
  }, log);
}
