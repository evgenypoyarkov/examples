/* jQ плагин, конструктор рекламных блоков(пример блока на mastertarot.ru) */

(function($, window, document, undefined) {
  var pluginName = 'blockcreator';
  var defaults = {
    propertyName: "value",
    preview: '#preview',
    presetsEmitter: '.js-preset',
    blockSelector: '.ctxtnblock',
    extraSettings: '.js-extra-settings',
    defaultPreset: 'default',
    /* мин. кол-во объявлений в блоке */
    size: {
      1: 3, /* центральный */
      2: 1, /* боковой - устарел */
      3: 5  /* меню - устарел */
    },
    common: {
      /* null - костыль. необходим для предотвращения добавления . к первому
         элементу. Так сложилось )
       */
      'null, html, body': {
        'margin': 0
      },
      'null, *, *:before, *:after': {
        '-webkit-box-sizing': 'border-box',
        '-moz-box-sizing': 'border-box',
        'box-sizing': 'border-box'
      },
      ctxtnblock: {
        'display': 'block',
        'overflow': 'hidden',
        'height': 'auto',
        'padding': '1px'
      },
      main: {
        'width': '100%',
        'height': '100%',
        'min-height': '65px',
        'float': 'left',
        'padding': '5px 10px 5px 10px',
        'line-height': '18px'
      },
      'ctxtna:first-of-type > .main': {
        'padding-top': '5px'
      },
      'ctxtna:last-of-type > .main': {
        'margin-bottom': 0
      },
      'main > div': {
        'margin-right': '45px'
      },
      'main:hover > .link-a': {
        'text-decoration': 'none !important',
      },
      'main:hover + .ctxtnarrow': {
        /* повторное декларирование цвета в принципе можно убрать,
           на данный момент цвет стрелки === цвету текста ссылки
        */
        color: '#0071ef',
        opacity: 1
      },
      'ctxtnarrow:hover': {
        color: '#0071ef',
        opacity: 1
      },
      ctxtnarrow: {
        'float': 'left',
        'width': '40px',
        'margin-left': '-50px',
        'margin-right': '10px',
        'font-size': '3.5em',
        'color': '#fff',
        'line-height': '1em',
        'opacity': '0.1'
      },
      'link-a': {
        cursor: 'pointer',
        'text-decoration': 'underline !important',
      }
    },
    presets: {
      default: {
        /* особые параметры пресета, контролы для которых отсутствуют на форме */
        specials: {
          ctxtnblock: {
            'cursor': 'pointer',
          },
          'main': {
            'border-top-width': '1px',
            'border-top-style': 'solid',
            'border-top-color': '#ececec'
          },
          'ctxtna:first-of-type > .main': {
            'border': 'none'
          },
          'main:hover > .link-a': {
            'text-decoration': 'none !important',
          },
          'main:hover + .ctxtnarrow': {
            color: '#0071ef',
            'opacity': 1
          },
          'ctxtnarrow:hover': {
            color: '#0071ef',
            opacity: 1
          },
        },
        /* прочие параметры, которые можно изменить через форму */
        css: {
          ctxtnblock: {
            'border-width': '1px',
            'border-style': 'solid',
            'border-color': '#ececec',
            'border-radius': '5px',
            'background-color': '#fff'
          },
          'link-a': {
            color: '#0071EF',
          },
        }
      },
      red: {
        specials: {
          'main': {
            'border-top-width': '1px',
            'border-top-style': 'solid',
            'border-top-color': '#ececec'
          },
          'ctxtna:first-of-type > .main': {
            'border': 'none'
          },
          'main:hover > .link-a': {
            'text-decoration': 'none !important',
          },
          'main:hover + .ctxtnarrow': {
            color: '#9A0000 !important',
            'opacity': 1
          },
          'ctxtnarrow:hover': {
            color: '#9A0000 !important',
            opacity: 1
          },
          'main:hover': {
            'background-color': '#FFF',
          },
        },
        css: {
          'ctxtnblock': {
            'border-width': '1px',
            'border-style': 'solid',
            'border-color': '#ececec',
            'border-radius': '5px',
            'background-color': '#F7F4FF',
          },
          'link-a': {
            color: '#9A0000',
          }
        }
      },
      gold: {
        specials: {
          'ctxtnarrow': {
            color: '#EBE2D1',
          },
          'main:hover > .link-a': {
            color: '#F4C381 !important',
            'text-decoration': 'underline !important',
          },
          'main:hover + .ctxtnarrow': {
            color: '#F4C381 !important',
            'opacity': 1
          },
          'ctxtnarrow:hover': {
            color: '#F4C381 !important',
            opacity: 1
          },
          'main': {
            'background-color': '#F9F8F4',
            'border-style': 'solid',
            'border-width': '1px',
            'border-color': '#ececec',
            'margin-top': '5px',
            '-webkit-box-shadow': '0px 1px 5px 1px #ccc',
            '-moz-box-shadow':    '0px 1px 5px 1px #ccc',
            'box-shadow':         '0px 1px 5px 1px #ccc',
          },
        },
        css: {
          'ctxtnblock': {
            'border-style': 'none',
            'background-color': '#FFF',
          },
          'link-a': {
            color: '#867853',
          },
        }
      },
      'default-sparse': {
        specials: {
          ctxtnblock: {
            'cursor': 'pointer',
          },
          'main': {
            'border-style': 'solid',
            'border-width': '1px',
            'border-color': '#ececec',
            'margin-top': '5px',
          },
          'main:hover > .link-a': {
            'text-decoration': 'none !important',
          },
          'main:hover + .ctxtnarrow': {
            color: '#0071ef',
            'opacity': 1
          },
          'ctxtnarrow:hover': {
            color: '#0071ef',
            opacity: 1
          },
        },
        css: {
          ctxtnblock: {
            'border-style': 'none',
            'background-color': '#fff'
          },
          'link-a': {
            color: '#0071EF',
          },
        }
      }
    }
  };

  var sizeIsValid = function(newSize, type) {
    return newSize >= defaults.size[type];
  };

  function Stash() {
    this.stash = {};
  }

  Stash.prototype.store = function(preset, css) {
    this.stash[preset] = css;
  };

  Stash.prototype.load = function(preset) {
    return this.stash[preset];
  };

  $.fn.serializeObject = function() {
      var o = {};
      var a = this.serializeArray();
      $.each(a, function() {
          if (o[this.name]) {
              if (!o[this.name].push) {
                  o[this.name] = [o[this.name]];
              }
              o[this.name].push(this.value || '');
          } else {
              o[this.name] = this.value || '';
          }
      });
      return o;
  };

  var eventHandlers = function(self) {
    return {
      onChange: function(e) {
        self.settings();

        switch (e.target.id) {
          case 'padding-y':
            var twin = $('#padding-y-bottom');
            break;
          case 'padding-x':
            var twin = $('#padding-x-right');
            break;
          case 'a-color':
            var twin = $('#arrow-color, #arrow-color-hover, #arrow-color-hover-self');
            break;
          case 'border-radius':
            if (self.params['border-style|ctxtnblock'] === 'none')
              var twin = $('.border-inner-radius');
            break;
          case 'type': 
            self.updateSize(e.target.value);
            break;
        }

        if (twin) 
          $(twin[0]).val(e.target.value);

        self.updatePreview();
      },
      onSubmit: function(e) {
        var payload = self.settings();
        payload.css = $('#preview-style > style').text();

        console.log(payload.css);
        e.data = {
          onSuccess: self.options.onSuccess || null,
          payload: payload
        };

        return ctxapi.eventHandlers.post(e);
      },
      setPreset: function(e) {
        self.currentPreset = e.target.value;
        if (self.currentPreset === 'custom') {
          $(self.extraSettings).show();
        }
        else {
          self.setJSON(self.options.presets[self.currentPreset].css);
          self.settings();
          $(self.extraSettings).hide();
        }
      }
    };
  }; 

  // конструктор плагина
  function Plugin(element, options) {
    this.element = element;
    this.options = $.extend({}, defaults, options) ;
    this._defaults = defaults;
    this._name = pluginName;
    this._handlers = eventHandlers(this);
    this.init();
  }

  Plugin.prototype.init = function() {
    var self = this;

    this.extraFields = $([
      'select[form="block-add"]', 
      'input[form="block-add"]:not(input[type="submit"])'
    ].join(', '));

    this.presetsEmitter = this.options.presetsEmitter;
    this.extraSettings = this.options.extraSettings;


    $(this.element).submit(this._handlers.onSubmit);
    $(this.presetsEmitter).on('change', this._handlers.setPreset);
    $(this.element).on('change', this._handlers.onChange);
    $(this.extraFields).on('change', this._handlers.onChange);
    $('.slider').slider().on('slide', this._handlers.onChange);
    $('.js-colorpicker').colorpicker().on('changeColor', function(e) {
      $(e.target).find('input').trigger('change');
    });

    this.dataProvider = null;
    this.stash = new Stash(); /* заготовка на memento */

    self.onLoad();
  };

  Plugin.prototype.settings = function() {
    this.block = $('.b-info').serializeObject();
    this.params = $('.p-param').serializeObject();
    return {
      block: this.block,
      params: this.params
    };
  };

  Plugin.prototype.updatePreview = function() {
    var self = this;
    var scope = {};
    self.settings();
        
    if (self.currentPreset !== 'custom') {
      $.extend(true,
               scope,
               self.options.common,
               self.options.presets[self.currentPreset].specials);
    }
    else {
      $.extend(true, scope, self.options.common);
    }
    self.css = self.generateCss(scope, self.params, true);
    $('#preview-style').html('<style>' + self.css + '</style>');
    self.renderPreview();
  };

  Plugin.prototype.onLoad = function() {
    var self = this;
    var text = $('#b-params').text();
    if (text !== '') {
      var params = JSON.parse(text);
      this.currentPreset = params.preset || 'custom';
      for (var el in params.s) {
        var selector = ['[name="', el, '"]'].join('');
        var $e = $(selector);

        if ($e.attr('type') === 'checkbox' && params.s[el])
          $e.attr('checked', true);
        else
          $e.val(params.s[el]);

        if ($e.hasClass('slider'))
          $e.slider('setValue', params.s[el]);

        if ($e.hasClass('js-colorpicker-input'))
          $e.parent().colorpicker('setValue', params.s[el]);
      }
    }

    if (!params) {
      this.currentPreset = this.options.defaultPreset;
      var preset = this.options.presets[this.currentPreset].css;
      this.setJSON(preset);
    }

    var pr = $(this.presetsEmitter);
    var elm = pr.filter(function(index) {
      return $(this).val() === self.currentPreset;
    });
    
    var li = elm.attr('checked', true)
    .parent()
    .addClass('active');

    if (this.currentPreset !== 'custom')
      $(this.extraSettings).hide();

    self.updateSize();
    self.updatePreview();
  };
  
  Plugin.prototype.deserialize = function(scope, data) {
    for (var el in data) {
      if (/\|/.test(el)) {
        el.split('&').forEach(function(s) {
          var a = s.split('|');
          var attr = a[0];
          var c = a[1];
          var v = a[2] || '';
          if (v !== 'px' && v !== '%' && v !== '') {
            v = $('#' + v).val();
          }
          if (typeof scope[c] === 'undefined') {
            scope[c] = {};
            scope[c][attr] = data[el] + v;
          }
          else {
            var tmp = scope[c][attr] ? scope[c][attr].split(' ') : null;
            if (!tmp || tmp[1] !== '!important') {
              scope[c][attr] = data[el] + v;
            }
          }
        });
      }
    }
  };

  var selectorByName = function(name) {
    return ['[name="', name, '"]'].join('');
  };

  Plugin.prototype.setJSON = function(config) {
    for (var selector in config) {
      for (var attribute in config[selector]) {
        var element = [attribute, selector].join('|');
        var value = config[selector][attribute];
        var granularity = value.match(/(px|%)$/);

        var defaultGranularityHandler = [selector, attribute, 'val'].join('-');
        var granularityHandlers = [
          selectorByName(defaultGranularityHandler)
        ];

        var elements = [
          selectorByName(element),
          selectorByName([element, defaultGranularityHandler].join('|'))
        ];

        if (granularity) {
          value = value.slice(0, granularity.index);
          granularity = granularity[0];
          var granularityHandler = [attribute, selector, granularity].join('-');
          var elementHandler = [element, granularity].join('|');

          granularityHandlers.push(selectorByName(granularityHandler));
          elements.push(selectorByName(elementHandler));
        }

        granularityHandlers = granularityHandlers.join(', ');
        elements = elements.join(', ');

        $(granularityHandlers).val(granularity);
        $(elements).val(value);

        if ($(elements).hasClass('slider'))
          $(elements).slider('setValue', value);

        if ($(elements).hasClass('js-colorpicker-input'))
          $(elements).parent().colorpicker('setValue', value);
      }
    }
  };

  Plugin.prototype.generateCss = function(scope, data, prettify) {
    var res = scope;
    var str = '';
    var t = prettify ? '    ' : '';
    var n = prettify ? '\n' : '';
    this.deserialize(scope, data);
    for (var el in scope) {
      str += '.' + el + '{' + n;
      for (var a in res[el]) {
        str += t + a + ':' + res[el][a] + ';' + n;
      }
      str += '}' + n;
    }
    return str;
  };

  Plugin.prototype.renderPreview = function() {
    var self = this;
    var body = '';
    var size = parseInt(self.block.size);
    var type = parseInt(self.block.type);

    if (!this.dataProvider) {
      this.dataProvider = new PreviewDataProvider({ default: 'real',
                                                    requred: size });
    }

    $(this._defaults.blockSelector).empty();

    this.dataProvider.data(size, function(data) {
      while (size-- && data.length) {
        body += self.renderAd(type, data.pop());
      }
      $(self._defaults.blockSelector).html(body);
    });
  };

  Plugin.prototype.renderAd = function(type, data) {
    var body = [
      '<a href="#" class="ctxtna link">',
        '<div class="main">',
          '<div class="link-a">',
             data.title,
          '</div>',
          ((type !== 3) ? '<div class="p-body">' + data.description + '</div>' : ''),
        '</div>',
        '<div class="ctxtnarrow">',
          '<span class="icon-right-open-1"></span>',
        '</div>',
      '</a>',
    ].join('\n');
    return body;
  };

  Plugin.prototype.updateSize = function(type) {
    var type = type || $('#type').val() || 1;
    var minAdsInBlock = this._defaults.size[type];
    var sizeControl = $('#size');
    $(sizeControl).attr('min', minAdsInBlock);
    if ($(sizeControl).val() < minAdsInBlock)
      $(sizeControl).val(minAdsInBlock);
  };

  function PreviewDataProvider(options) {
    var self = this;
    this.required = options.required || 5;
    this._data = [];
    this._providers = {
      static: function(callback) {
        /* не протестировано */
        var data = [];
        var required = self.required;
        while (required--) {
          data.push({ title: 'Заголовок', description: 'Текст объявления' });
        }
        callback(data);
      },
      real: function(callback) {
        $.getJSON('/materials/preview', {
          required: self.required
        })
        .done(callback)
        .fail(callback);
      }
    }
    this.default = this._providers[options.default] || this._providers.static;
  }

  /* установить количество объявлений, которые необходимо получить от сервера
   * для первью
   */
  PreviewDataProvider.prototype.setRequired = function(value) {
    this.required = value;
  };

  PreviewDataProvider.prototype.data = function(required, callback) {
    var self = this;
    var balance = required - this._data.length;
    if (this._data.length >= required) {
      return callback(this._data.slice(0, required));
    }
    else {
      this.setRequired(balance);
      return this.default(function(data) {
        while (data.length)
          self._data.push(data.pop());
        callback(self._data.slice(0, required));
      });
    }
  };

  $.fn[pluginName] = function(options) {
    return this.each(function() {
      if (!$.data(this, 'plugin_' + pluginName)) {
        $.data(this, 'plugin_' + pluginName, new Plugin(this, options));
      }
    });
  };
})(jQuery, window, document);
